N = 100

#falseを裏とする
cards = Array.new(N){false}

(2..N).each {|e|
    cards.each_with_index {|card, i|
        if e <= (i+1) && (i+1)%e==0
            cards[i] = !card;
        end
    }
}

cards.each_with_index{|card, i|
    puts i+1 unless card
}
