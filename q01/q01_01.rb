num = 11
while true
    if num.to_s == num.to_s.reverse &&
        num.to_s(2) == num.to_s(2).reverse &&
        num.to_s(8) == num.to_s(8).reverse

       puts num
       break
    end

#  2進数の時、1桁目が0(つまり偶数)だと、一番上の桁も0になる。
#  これは2進数として存在し得ないので奇数のみでOK。
   num += 2
end
